import math
import pandas_datareader as web
import numpy as np
import pandas as pd
import pytse_client as tse

from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM
import keras
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')


#df = web.DataReader('AAPL' , data_source='yahoo' , start='2012-01-01'  , end='2020-11-11')

#data = tse.download(symbols = "فولاد" , write_to_csv= True)
data = pd.read_csv('output.csv')
#data.drop(['<TICKER>' , '<PER>'], axis=1 , inplace=True)
#new_col ={'<DTYYYYMMDD>' : 'Date' , '<FIRST>' : 'First' ,
#'<OPENINT>' : 'OpenInt' , '<OPEN>' : 'Open', '<HIGH>' : 'High' ,
#'<LOW>': 'Low' , '<CLOSE>' : 'Close' ,'<VALUE>' : 'Value' , '<VOL>' : 'Volume' }
#data.rename(columns= new_col , inplace= True)
#data['Date'] = pd.to_datetime(data['Date'] , format='%Y%m%d')
#data = data.set_index('Date')

#print(data)




#plt.figure(figsize=(11,4))
#plt.title('Close Price History')
#plt.plot(data['Close'])
#plt.xlabel('Date',fontsize=13)
#plt.ylabel('Close Price',fontsize=13)
#plt.show()

x_test_index_param = 50

#find training data len
df = data.filter(['close'])
dataset = df.values
training_data_len = math.ceil( len(dataset) *.90)

#scale data btw 0-1
scaler = MinMaxScaler(feature_range=(0, 1)) 
scaled_data = scaler.fit_transform(dataset)
train_data = scaled_data[0:training_data_len  , : ]

#Split the data into x_train and y_train data sets , (tip = images)
x_train=[]
y_train = []
for i in range(x_test_index_param,len(train_data)):
    x_train.append(train_data[i-x_test_index_param:i,0])
    y_train.append(train_data[i,0])

#convert to numpy array
x_train, y_train = np.array(x_train), np.array(y_train)

#reshape to accepted shape By LSTM
x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],1))


model = Sequential()
model.add(LSTM(units=128,activation = 'linear', return_sequences=True,input_shape=(x_train.shape[1],1)))
model.add(LSTM(units=64, return_sequences=False ))
model.add(Dense(units=16))
model.add(Dense(units=1 ))


model.compile(optimizer='adam', loss='mean_squared_error')
model.fit(x_train, y_train, batch_size=5, epochs=1)

model.save('model.h5')




test_data = scaled_data[training_data_len - x_test_index_param: , : ]
x_test = []
y_test =  dataset[training_data_len : , : ] 
for i in range(x_test_index_param,len(test_data)):
    x_test.append(test_data[i-x_test_index_param:i,0])

x_test = np.array(x_test)
x_test = np.reshape(x_test, (x_test.shape[0],x_test.shape[1],1))

predictions = model.predict(x_test) 
predictions = scaler.inverse_transform(predictions)

rmse=np.sqrt(np.mean(((predictions- y_test)**2)))


#Plot/Create the data for the graph
train = data[:training_data_len]
valid = data[training_data_len:]
valid['Predictions'] = predictions

#Visualize the data
plt.figure(figsize=(10,4))
plt.title('Model')
plt.xlabel('Date', fontsize=13)
plt.ylabel('Close Price ', fontsize=13)
plt.plot(train['close'])
plt.plot(valid[['close', 'Predictions']])
plt.legend(['Train', 'Val', 'Predictions'], loc='lower right')
plt.show()



print(valid)



#Get the quote
#Create a new dataframe
new_df = data.filter(['close'])
#Get teh last 60 day closing price 
last_60_days = new_df[-x_test_index_param:].values
#Scale the data to be values between 0 and 1
last_60_days_scaled = scaler.transform(last_60_days)
#Create an empty list
X_test = []
#Append teh past 60 days
X_test.append(last_60_days_scaled)
#Convert the X_test data set to a numpy array
X_test = np.array(X_test)
#Reshape the data
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
#Get the predicted scaled price
pred_price = model.predict(X_test)
#undo the scaling 
pred_price = scaler.inverse_transform(pred_price)
print()
print()
print(pred_price)