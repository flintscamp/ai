import numpy as np
import pandas as pd
import os
import seaborn as sns
import random
import copy
import matplotlib.pyplot as plt
import time

data = pd.read_csv('data/FEOLAD.csv')

data.drop(['<Ticker>' , '<Per>', '<TIME>'], axis=1 , inplace=True)

new_col ={'<DTYYYYMMDD>' : 'Date' , '<OPEN>' : 'Open', '<HIGH>' : 'High' ,
 '<LOW>': 'Low' , '<CLOSE>' : 'Close' , '<VOL>' : 'Volume' }
data.rename(columns= new_col , inplace= True)

data['Date'] = pd.to_datetime(data['Date'] , format='%Y%m%d')
data = data.set_index('Date')


df = pd.DataFrame(data)
data_np = df.to_numpy()


n1 = int(data_np.shape[0] * 0.8)
n2 = int((data_np.shape[0] - n1)/2)
x_train = data_np[:n1]
x_val = data_np[n1: n1 + n2]
x_test = data_np[n1 + n2:]



from sklearn import preprocessing
minmax_scale = preprocessing.MinMaxScaler().fit(x_train)
x_train_n = minmax_scale.transform(x_train)
x_val_n = minmax_scale.transform(x_val)
x_test_n = minmax_scale.transform(x_test)


def slicing_30(x, history_points):
    #iloc[row slicing, column slicing]
    sliced_data = np.array([x[i  : i + history_points] for i in range(len(x) - history_points)])
    # iloc[row slicing, column slicing]
    labels = np.array([x[:,0][i + history_points] for i in range(len(x) - history_points)])
    return sliced_data, labels

history_points = 1
x_train_n, y_train = slicing_30(x_train_n, history_points)
x_val_n, y_val = slicing_30(x_val_n, history_points)
x_test_n, y_test = slicing_30(x_test_n, history_points)



##import scikit

##randomForest

import keras
import tensorflow as tf
from keras.models import Model,Sequential
from keras.layers import Dense, Dropout, LSTM, Input, Activation, concatenate
from keras import optimizers
import numpy as np

model = Sequential()
model.add(LSTM(32 , activation = 'relu' ,input_shape = (history_points , 5)))
model.add(Dropout(0.1))
model.add(Dense(64 , activation= 'relu'))
model.add(Dropout(0.2))
model.add(Dense(32, activation= 'relu'))
model.add(Dense(1,activation='sigmoid'))

adam = optimizers.Adam(lr=0.01)
model.compile(optimizer=adam,  loss='binary_crossentropy',  metrics=['accuracy'])

model.fit(x_train_n , y_train , epochs = 5)

#model.save('firstjump.h5')

loss , acc = model.evaluate(x_test_n , y_test )

print(acc)




