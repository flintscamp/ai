import math
import pandas_datareader as web
import numpy as np
import pandas as pd
import pytse_client as tse

from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM
import keras
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')


#df = web.DataReader('AAPL' , data_source='yahoo' , start='2012-01-01'  , end='2020-11-11')

#data = pd.read_csv('tickers_data/فولاد.csv')
data = pd.read_csv('output.csv')
data= data.drop('date', 1)
#new_col ={'<DTYYYYMMDD>' : 'Date' , '<FIRST>' : 'First' ,
#'<OPENINT>' : 'OpenInt' , '<OPEN>' : 'Open', '<HIGH>' : 'High' ,
#'<LOW>': 'Low' , '<CLOSE>' : 'Close' ,'<VALUE>' : 'Value' , '<VOL>' : 'Volume' }
#data.rename(columns= new_col , inplace= True)
#data['Date'] = pd.to_datetime(data['Date'] , format='%Y%m%d')
#data = data.set_index('Date')

#print(data)




#plt.figure(figsize=(11,4))
#plt.title('Close Price History')
#plt.plot(data['Close'])
#plt.xlabel('Date',fontsize=13)
#plt.ylabel('Close Price',fontsize=13)
#plt.show()

x_test_index_param = 50

#find training data len
df = data.filter(['close'])
fulldataset = data.values
closedataset = df.values
training_data_len = math.ceil( len(closedataset) *.90)


scaler = MinMaxScaler(feature_range=(0, 1)) 
scaled_data_full = scaler.fit_transform(fulldataset)
scaled_data_close = scaler.fit_transform(closedataset)
train_data_full = scaled_data_full[0:training_data_len  , : ]
train_data_close = scaled_data_close[0:training_data_len  , : ]


x_train = []
y_train = []

for i in range(x_test_index_param , len(train_data_close) ):
    x_train.append(train_data_full[i-x_test_index_param:i,:])
    y_train.append(train_data_close[i,0])


#print(len(x_train[0]))
#print(len(x_train[0][0]))
#print()
#print(y_train[0])


x_train, y_train = np.array(x_train), np.array(y_train)
#print(x_train[0])
#print(y_train[0])
x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],x_train.shape[2]))


model = Sequential()
model.add(LSTM(units=128,activation = 'linear', return_sequences=True,input_shape=(x_train.shape[1],x_train.shape[2])))
model.add(LSTM(units=64, return_sequences=False ))
model.add(Dense(units=8))
model.add(Dense(units=1))


model.compile(optimizer='adam', loss='mean_squared_error')
model.fit(x_train, y_train, batch_size=7 , epochs=1)




test_data_full = scaled_data_full[training_data_len - x_test_index_param: , : ]
test_data_close = scaled_data_close[training_data_len - x_test_index_param: , : ]
x_test = []
y_test =  closedataset[training_data_len : , : ] 
for i in range(x_test_index_param,len(test_data_full)):
    x_test.append(test_data_full[i-x_test_index_param:i,:])

x_test = np.array(x_test)
x_test = np.reshape(x_test, (x_test.shape[0],x_test.shape[1],x_test.shape[2]))

predictions = model.predict(x_test) 
predictions = scaler.inverse_transform(predictions)

rmse=np.sqrt(np.mean(((predictions- y_test)**2)))


#Plot/Create the data for the graph
train = data[:training_data_len]
valid = data[training_data_len:]
valid['Predictions'] = predictions

#Visualize the data
plt.figure(figsize=(10,4))
plt.title('Model')
plt.xlabel('Date', fontsize=13)
plt.ylabel('Close Price ', fontsize=13)
plt.plot(train['close'])
plt.plot(valid[['close', 'Predictions']])
plt.legend(['Train', 'Val', 'Predictions'], loc='lower right')
plt.show()



print(valid)